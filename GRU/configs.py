# import LSTM.utils as utils
from utils import *

# prepare input data
input = preprocess_sentence("./data/input_pun")
input_tokenizer, input_tensor ,input_max_length = tokenize(input)

# prepare output data
target = preprocess_sentence("./data/output_pun")
target_tokenizer, target_tensor ,target_max_length = tokenize(target)

# data shapes
print(input_tensor.shape)
print(target_tensor.shape)

def convert(tokenizer, tensor):
    '''
    DESCRIPTION:
    This function to convert index to word for input tensor
    INPUT: 
    tokenizer: object of converted text into a sequence of integer
    tensor: list of integer
    OUTPUT: 
    None
    ''' 
    for t in tensor:
        if t!=0:
              print ("%d ----> %s" % (t, tokenizer.index_word[t]))
    
print ("Input Language; index to word mapping")
convert(input_tokenizer, input_tensor[0])
print ("Output Language; index to word mapping")
convert(target_tokenizer, target_tensor[0]) 

BUFFER_SIZE = len(input_tensor)
BATCH_SIZE = 256
steps_per_epoch = len(input_tensor)//BATCH_SIZE
embedding_dim = 128
units = 1024
vocab_inp_size = len(input_tokenizer.word_index)+1
vocab_tar_size = len(target_tokenizer.word_index)+1

# create dataset
train_dataset = tf.data.Dataset.from_tensor_slices((input_tensor, target_tensor)).shuffle(BUFFER_SIZE)
train_dataset = train_dataset.batch(BATCH_SIZE, drop_remainder=True)

example_input_batch, example_target_batch = next(iter(train_dataset))
example_input_batch.shape, example_target_batch.shape

optimizer = tf.keras.optimizers.Adam()
loss_object = tf.keras.losses.SparseCategoricalCrossentropy(
    from_logits=True, reduction='none')

def loss_function(real, pred):
    mask = tf.math.logical_not(tf.math.equal(real, 0))
    loss_ = loss_object(real, pred)

    mask = tf.cast(mask, dtype=loss_.dtype)
    loss_ *= mask

    return tf.reduce_mean(loss_)
    
