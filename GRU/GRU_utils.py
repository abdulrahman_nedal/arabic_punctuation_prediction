import tensorflow as tf
import time
from configs import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import os
import numpy as np
from tqdm import tqdm

def train_step(model, inp, targ, enc_hidden):
    '''
    DESCRIPTION:
    This function to train encode-decode model
    INPUT: 
    inp: input vector
    targ: target vector
    enc_hidden: encoder initial hidden state
    OUTPUT:
    batch_loss: train loss
    ''' 

    loss = 0
    accuracy=0
    with tf.GradientTape() as tape:
        enc_output, enc_hidden = model.encoder(inp, enc_hidden)

        dec_hidden = enc_hidden

        dec_input = tf.expand_dims([target_tokenizer.word_index['<start>']] * BATCH_SIZE, 1)

        # Teacher forcing - feeding the target as the next input
        for t in range(1, targ.shape[1]):
          # passing enc_output to the decoder
          predictions, dec_hidden, _ = model.decoder(dec_input, dec_hidden, enc_output)

          loss += loss_function(targ[:, t], predictions)

          # using teacher forcing
          dec_input = tf.expand_dims(targ[:, t], 1)

    batch_loss = (loss / int(targ.shape[1]))

    variables = model.encoder.trainable_variables + model.decoder.trainable_variables

    gradients = tape.gradient(loss, variables)

    optimizer.apply_gradients(zip(gradients, variables))

    return batch_loss

def train(epoches, model):
    checkpoint_dir = 'gru_training_checkpoints'
    checkpoint_prefix = os.path.join(checkpoint_dir, "ckpt")
    checkpoint = tf.train.Checkpoint(optimizer=optimizer,
                                    encoder=model.encoder,
                                    decoder=model.decoder)

    for epoch in range(epoches):
        start = time.time()

        enc_hidden = model.encoder.initialize_hidden_state()
        total_loss = 0

        for (batch, (inp, targ)) in enumerate(train_dataset.take(steps_per_epoch)):
            batch_loss = train_step(model,inp, targ, enc_hidden)
            total_loss += batch_loss

            if batch % 100 == 0:
                print('Epoch {} Batch {} Loss {:.4f}'.format(epoch + 1,
                                                            batch,
                                                            batch_loss.numpy()))
        # saving (checkpoint) the model every 2 epochs
        if (epoch + 1) % 2 == 0:
            checkpoint.save(file_prefix = checkpoint_prefix)

        print('Epoch {} Loss {:.4f}'.format(epoch + 1,
                                            total_loss / steps_per_epoch))
        print('Time taken for 1 epoch {} sec\n'.format(time.time() - start))

# function for plotting the attention weights
def plot_attention(attention, sentence, predicted_sentence):
  '''
    DESCRIPTION:
    This function to plot attention 
    INPUT: 
    attention: attention weights
    sentence: input sentence 
    predicted_sentence: predict result

    OUTPUT: 
    None
    ''' 
  fig = plt.figure(figsize=(10,10))
  ax = fig.add_subplot(1, 1, 1)
  ax.matshow(attention, cmap='viridis')

  fontdict = {'fontsize': 14}

  ax.set_xticklabels([''] + sentence, fontdict=fontdict, rotation=90)
  ax.set_yticklabels([''] + predicted_sentence, fontdict=fontdict)

  ax.xaxis.set_major_locator(ticker.MultipleLocator(1))
  ax.yaxis.set_major_locator(ticker.MultipleLocator(1))

  plt.show()

def evaluate(model, sentence):
    '''
    DESCRIPTION:
    This function to predict result
    INPUT: 
    sentence: input sentence 
    OUTPUT: 
    result: predict result
    sentence: input sentence 
    attention_plot: attention weights
    ''' 
    attention_plot = np.zeros((target_max_length, input_max_length))

    #sentence = preprocess_sentence(sentence)
    sentence = '<start> '+sentence + ' <end>'
    
    inputs = [input_tokenizer.texts_to_sequences([i])[0][0] for i in sentence.split(' ')]
    inputs = tf.keras.preprocessing.sequence.pad_sequences([inputs],
                                                         maxlen=input_max_length,
                                                         padding='post')
    inputs = tf.convert_to_tensor(inputs)
    
    result = ''

    hidden = [tf.zeros((1, units))]
    enc_out, enc_hidden = model.encoder(inputs, hidden)
    dec_hidden = enc_hidden
    dec_input = tf.expand_dims([target_tokenizer.word_index['<start>']], 0)

    for t in range(target_max_length):
        predictions, dec_hidden, attention_weights = model.decoder(dec_input,
                                                         dec_hidden,
                                                         enc_out)

        # storing the attention weights to plot later on
        attention_weights = tf.reshape(attention_weights, (-1, ))
        attention_plot[t] = attention_weights.numpy()

        predicted_id = tf.argmax(predictions[0]).numpy()

        result += target_tokenizer.index_word[predicted_id] + ' '

        if target_tokenizer.index_word[predicted_id] == '<end>':
            return result, sentence, attention_plot

        # the predicted ID is fed back into the model
        dec_input = tf.expand_dims([predicted_id], 0)

    return result, sentence, attention_plot

def predict(model, sentence):
    '''
    DESCRIPTION:
    This function to predict output sentence

    INPUT: 
    sentence: input sentence 

    OUTPUT: 
    None
    ''' 
    result, sentence, attention_plot = evaluate(model, sentence)

    print('Input:\n %s' % (sentence))

    predict = ''
    sentence_list = sentence.split(' ')
    sentence_list.pop(0) # remove <start>
    result_list = result.split(' ')

    for i in range(len(sentence_list)):
      if (result_list[i]=='space'):
        predict += sentence_list[i]+' '
      else:
        predict += sentence_list[i]+result_list[i]+' '
    print('Predicted punctuation:\n {}'.format(predict))

    attention_plot = attention_plot[:len(result.split(' ')), :len(sentence.split(' '))]
    plot_attention(attention_plot, sentence_list, result_list)


def evaluate_model(model, sentences, target):
    '''
    DESCRIPTION:
    This function to evaluate model 
    INPUT: 
    sentences: input vector
    target: target vector
    OUTPUT: 
    actual: real target sentences
    predicted: predict target sentences
    ''' 
    actual, predicted = list(), list()
    outer = tqdm(range(len(sentences)),leave=True,position =0)
    for i, sentence in enumerate(sentences):
        # translate encoded source text
        #sentence = sentence.reshape((1, sentence.shape[0]))
        predict,raw_src,_ = evaluate(model, sentence)
        predict = predict.replace(' <end> ','')
        raw_target = target[i]        
        print('src=[%s], target=[%s], predicted=[%s]' % (sentence, raw_target, predict))
        actual.append([raw_target.split()])
        predicted.append(predict.split())
        outer.update(1)

    return actual, predicted