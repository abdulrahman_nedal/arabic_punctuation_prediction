import tensorflow as tf

def preprocess_sentence(file_name):
    '''
    DESCRIPTION:
    This function to read the file, then add <start> and <end> as a tag in begin and end for each sequence.
    INPUT: 
    file_name: file name
    OUTPUT: 
    text: text after preprocess
    ''' 
    with open(file_name , 'r', encoding='windows-1256') as f:
        text = f.readlines()  
    text = ['<start> '+ t.replace('\n','') + ' <end>' for t in text]
    return text

def calculate_max_length(tensor):
    '''
    DESCRIPTION:
    This function to Calculates the max length in tensor
    INPUT: 
    tensor: input tensor
    OUTPUT: 
    _: max length of tensor
    ''' 
    return max(len(t) for t in tensor)

def tokenize(text):
    '''
    DESCRIPTION:
    This function to convert inputs to numeric sequences with the maximum length
    INPUT: 
    text: list of string
    OUTPUT: 
    tokenizer: object of converted text into a sequence of integer
    text_vector: vector of converted text into a sequence of integer
    max_length: max length in text_vector
    ''' 

    # Choose the top 9000 words from the vocabulary
    top_k = 9000
    tokenizer = tf.keras.preprocessing.text.Tokenizer(num_words=top_k, oov_token="<unk>",filters='')
    tokenizer.fit_on_texts(text)

    #add a word for padding 
    tokenizer.word_index['<pad>'] = 0
    tokenizer.index_word[0] = '<pad>'

    # Create the tokenized vectors
    text_seqs = tokenizer.texts_to_sequences(text)

    # Pad each vector to the max_length of the vector
    text_vector = tf.keras.preprocessing.sequence.pad_sequences(text_seqs, padding='post')

    # Calculates the max_length, which is used to store the attention weights
    max_length = calculate_max_length(text_seqs)
    
    return tokenizer, text_vector,max_length