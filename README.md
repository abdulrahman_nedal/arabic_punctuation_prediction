# arabic_punctuation_prediction

## 1. Project Overview <a name="ProjectOverview"></a> 
Punctuations are symbols used to organize written textual to make it clear and easy to read. Lack of punctuation could cause confusion and misunderstanding for the reader. The goal of the project is to build a seq2seq model capable of predicting the locations of punctuation marks and recovering the missing ones.

## 2. Installation <a name="installation"></a>

- Python versions 3.*.
- Python Libraries:
    - matplotlib.
    - tensorflow.
    - sklearn.
    - Pandas.
    - keras.
    - numpy.
    - string.
    - nltk
    - tqdm
    - time.
    - re.

## 3. Data Exploration and Visualization <a name="data"></a> 

[Tashkeela](https://www.kaggle.com/linuxscout/tashkeela) dataset, the dataset contains over 75 Arabic words obtained from 97 books. Also, it contains different types of punctuation. So, we will use it in this project.
We focus on five punctuation marks: comma, dot, semicolon, question mark, two vertical points. Below are some texts from Al-Bahr Al-Muhit book.

## 4. Implementation <a name="model"></a> 
In this project, we used the sequence to sequence (seq2seq) model the same technique used of [Neural Machine Translation (NMT)](https://www.tensorflow.org/tutorials/text/nmt_with_attention) provided by TensorFlow. The inputs pass through the encoder model to give s us the encoder output and encoder hidden state. we built two models ,one based on gru, and the other based on lstm [Bahdanau Attention](https://arxiv.org/pdf/1409.0473.pdf) has been used for the encoder.

## 5. Result and Metrics<a name="results"></a> 
We trained our gru model using 20 epoch with 128 batch size. We use categorical cross-entropy loss function and Adam optimizer. A [Bilingual Evaluation Understudy (BLEU)](https://www.aclweb.org/anthology/P02-1040.pdf). In the below figure, the results of the prediction for two examples.
![128](https://user-images.githubusercontent.com/42017072/74445418-688f6180-4e87-11ea-85f3-a6b0a3371822.png)

## 6. Acknowledgements <a name="acknowledgements"></a> 
I wish to thank [ ZarahShibli ](https://github.com/ZarahShibli) because my project is based on her project. For more details about this project can read this for ZarahShibli [article](https://medium.com/@ZarahShibli/what-comes-after-the-word-61c5adc9b8a0).



