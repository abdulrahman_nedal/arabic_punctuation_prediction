import tensorflow as tf 
from configs import *
 
class LSTM_Encoder(tf.keras.Model):
    def __init__(self, vocab_size, embedding_dim, enc_units, batch_sz):
        super(LSTM_Encoder, self).__init__()
        self.batch_sz = batch_sz
        self.enc_units = enc_units
        self.embedding = tf.keras.layers.Embedding(vocab_size, embedding_dim)
        self.lstm = tf.keras.layers.LSTM(self.enc_units,
                                       return_sequences=True,
                                       return_state=True,
                                       recurrent_initializer='glorot_uniform')

    def call(self, x, hidden):
        x = self.embedding(x)
        output, state_h, state_c = self.lstm(x, initial_state = hidden)
        return output, [state_h, state_c]

    def initialize_hidden_state(self):
        return [tf.zeros((self.batch_sz, self.enc_units)),tf.zeros((self.batch_sz, self.enc_units))]

class LSTM_Decoder(tf.keras.Model):
    def __init__(self, vocab_size, embedding_dim, dec_units, batch_sz):
        super(LSTM_Decoder, self).__init__()
        self.batch_sz = batch_sz
        self.dec_units = dec_units
        self.embedding = tf.keras.layers.Embedding(vocab_size, embedding_dim)
        self.lstm = tf.keras.layers.LSTM(self.dec_units,
                                       return_sequences=True,
                                       return_state=True,
                                       recurrent_initializer='glorot_uniform')
        self.fc = tf.keras.layers.Dense(vocab_size)

    def call(self, x, hidden):
        x = self.embedding(x)
        output, state_h, state_c = self.lstm(x, initial_state = hidden)        
        x = self.fc(output)        

        return x, [state_h, state_c]

class LSTM_model:
    def __init__(self) -> None:
        self.encoder = LSTM_Encoder(vocab_inp_size, embedding_dim, units, BATCH_SIZE)
        # sample input
        sample_hidden = self.encoder.initialize_hidden_state()

        self.sample_output, self.sample_hidden = self.encoder(example_input_batch, sample_hidden)

        self.decoder = LSTM_Decoder(vocab_tar_size, embedding_dim, units, BATCH_SIZE)

        self.sample_decoder_output, _= self.decoder(tf.random.uniform((BATCH_SIZE, 1)), sample_hidden)