from GRU_model import GRU_model
from GRU_utils import predict
import tensorflow as tf
from configs import *

model = GRU_model()
checkpoint = tf.train.Checkpoint(optimizer=optimizer,
                                    encoder=model.encoder,
                                    decoder=model.decoder)

checkpoint_dir = 'gru_training_checkpoints'
checkpoint.restore(tf.train.latest_checkpoint(checkpoint_dir))

real_sentence = 'قال محمد : السلام عليكم'
# real_sentence = 'نجح محمد في الامتحان ؛ لأنه اجتهد في دروسه.'
test_sentence = 'قال محمد السلام عليكم'
print('Original Text: ',real_sentence)

in_seq = test_sentence.strip().split(' ')
n =10
in_sequances = [" ".join(in_seq[i:i+n]) for i in range(0, len(in_seq), n)]
[predict(model, s) for s in in_sequances]